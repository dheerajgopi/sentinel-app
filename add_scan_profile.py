#! virtenv/bin/python
""" modifies the config.py of the sentinel app """
from werkzeug import generate_password_hash
from pymongo import MongoClient
from getpass import getpass
from datetime import datetime

def add_scan_profile():
    mongo_host = raw_input("Enter the mongodb host: ")
    mongo_port = raw_input("Enter the mongodb port: ")
    mongo_dbname = raw_input("Enter the mongodb db name: ")

    client = MongoClient(mongo_host, int(mongo_port))

    db = client[mongo_dbname]

    scan_profile_collection = db.scanProfiles

    url = raw_input("Enter url name: ")
    target_url = raw_input("Enter target url: ")
    spider_seed = raw_input("Enter spider seed: ")
    context_url = raw_input("Enter context url: ")
    login_url = raw_input("Enter login indicator: ")
    logout_url = raw_input("Enter logout indicator: ")
    username = raw_input("Enter username: ")
    password = raw_input("Enter password: ")
    username_tag = raw_input("Enter username html name tag: ")
    password_tag = raw_input("Enter password html name tag: ")

    scan_profile_data = {
                         "url": url,
                         "target_url": target_url,
                         "spider_seed": spider_seed,
                         "context_url": context_url,
                         "login_url": login_url,
                         "logout_url": logout_url,
                         "username": username,
                         "password": password,
                         "username_tag": username_tag,
                         "password_tag": password_tag
                    }
    scan_profile_collection.insert_one(scan_profile_data)

if __name__ == "__main__":
    add_scan_profile()
