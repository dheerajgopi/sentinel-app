from flask.ext.wtf import Form

# Import Form elements
from wtforms import SelectField, StringField

class ScanlistForm(Form):
    """
    Form to give a list of dates on which scans are performed for a given site.
    """
    date = SelectField('scans')
    site = StringField('site')
