""" models used by the sentinel application """

from sentinel import db
from sentinel.utils import xmltojson

class Report(db.Document):
    """ a report document """
    OWASPZAPReport = db.DynamicField()

    def insert(self, json_report):
        """ inserts the report to the database """
        self.OWASPZAPReport= json_report['OWASPZAPReport']
        self.save()

    def __repr__(self):
        return "<Report %r at %r >" % (self.OWASPZAPReport['site']['@name'], self.OWASPZAPReport['@generated'])
