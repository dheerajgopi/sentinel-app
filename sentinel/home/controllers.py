""" home controllers """

# Flask imports
from flask import Blueprint, render_template, request,\
                  redirect, url_for, session, jsonify, flash, abort
from flask.ext.login import login_user , logout_user , current_user , login_required

# Import the database object from the main app module
from sentinel import db, app

# Import models
from .models import Report

# Import forms
from sentinel.home.forms import ScanlistForm

# Import utility functions
from sentinel.utils.extract_info import risk_histo, uri_histo, top_of_hist,\
     frequency_of_alerts
from sentinel.utils.auth import get_user
from sentinel.utils.home import fetch_report, fetch_scan_dates, fetch_alert_list

# Create blueprint
home = Blueprint('home', __name__)


# ************ROUTES**************************************#
@home.route('/sites', methods=['GET', 'POST'])
@login_required
def homepage():
    """
    The homepage controller checks if the given 'username' is in the session.
    If the user is in the session, the homepage is shown, else, it shows an
    'unauthorised' message.
    """
    if not current_user:
        abort(404)

    if current_user.isAdmin:
        return redirect(url_for("admin.admin_home"))

    return render_template("home/sites.html", user=current_user)


@home.route('/sites/<site_index>', methods=['GET', 'POST'])
@login_required
def scanlist(site_index):
    """
    POST request:
    The page shows a list of dates on which scans are performed for a particular
    site. If there were no scan performed, it shows that as a message to the user.
    """
    try:
        site = current_user.ownedSites[int(site_index)]
    except:
        return abort(404)

    dates = fetch_scan_dates(site)

    if len(dates) > 0:
        form = ScanlistForm()
        form.date.choices = dates
        return render_template("home/scanlist.html", site=site, form=form)

    flash("No scans performed yet for this site.", "error")
    return redirect(url_for("home.homepage"))


@home.route('/getSummaryData', methods=['POST'])
@login_required
def get_summary_data():
    """
    Returns a json with the data required to generate the summary.
    """
    form = ScanlistForm()
    report = fetch_report(form.site.data, form.date.data)
    report_data = report.OWASPZAPReport
    report_id = str(report.id)
    risk_hist = risk_histo(report_data)
    alerts = frequency_of_alerts(report_data)
    top10_uris = top_of_hist(uri_histo(report_data), 10)
    context = {'risk_levels': risk_hist, 'alerts': alerts, 'top10_uris':top10_uris, 'report_id': report_id}
    return jsonify(context)


@home.route('/full_report/<report_id>', methods=['GET', 'POST'])
@login_required
def get_full_report(report_id):
    report = fetch_alert_list(report_id)
    return render_template("home/full-report.html", report=report)
