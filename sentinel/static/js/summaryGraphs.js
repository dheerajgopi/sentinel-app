function showGraphs(){
    var scanSite = document.getElementById("site").value;
    var scanDate = document.getElementById("date").value;
    $.post('/getSummaryData',
        {
            site: scanSite,
            date: scanDate
        },
        function(data, status) {
            var link = "/full_report/" + data.report_id;

            document.getElementById('fullreportlink').href = link;
            document.getElementById('fullreportlink').style.visibility = 'visible';

            $('#risklevels').highcharts(riskPie(data.risk_levels));
            $('#alerttypes').highcharts(alertsBar(data.alerts));
            $('#vulnerableuris').highcharts(vulnerableSitesBar(data.top10_uris));
        }
    );
}

function riskPie(inputData) {
    var pieChartData = [];
    for (var risk in inputData) {
        if (inputData.hasOwnProperty(risk)) {
            pieChartData.push({name: risk, y: inputData[risk]});
        }
    }
    var chart = {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    };

    var plotOptions = {
        pie: {
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    };

    var title = { text: 'Risk Levels' };

    var series = [{
        type: 'pie',
        name: 'Risks',
        data: pieChartData
    }];

    var pieJSON = {};
    pieJSON.chart = chart;
    pieJSON.title = title;
    pieJSON.series = series;
    pieJSON.plotOptions = plotOptions;
    return pieJSON;
}

function alertsBar(barData) {
    var barGraphData = [];
    var xAxisScale = [];
    for (var i = 0; i < barData.length; i++) {
        barGraphData.push(Number(barData[i].count));
        xAxisScale.push(barData[i].alert);
    }

    var chartData = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Error Count'
        },
        subtitle: {
            text: 'OWASP ZAP Report'
        },
        xAxis: {
            categories: xAxisScale
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count',
                align: 'low'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: false
                }
            }
        },

        credits: {
            enabled: false
        },
        series: [{
            name: 'Error count',
            data: barGraphData}]
        };

    return chartData;
}

function vulnerableSitesBar(barData) {
    var barGraphData = [];
    var xAxisScale = [];
    for (var i = 0; i < barData.length; i++) {
        barGraphData.push(Number(barData[i].count));
        xAxisScale.push(barData[i].uri);
    }
    var chartData = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Most Vulnerable URIs'
        },
        subtitle: {
            text: 'OWASP ZAP Report'
        },
        xAxis: {
            categories: xAxisScale
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count',
                align: 'low'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: false
                }
            }
        },

        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of vulnerabilities',
            data: barGraphData}]
        };
    return chartData;
}
