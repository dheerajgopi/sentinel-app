function checkScanFailure() {
    $(document).ready(function() {
        setTimeout(function() {
            $.get("/admin/getScanState", function(data, status) {
                if (data['status'] === 'FAILURE') {
                    alert("Scan terminated! Either admin stopped the scan or ZAP faced some unexpected issues");
                }
            });
        }, 1000);
    });
}

function scanStats() {
    $(document).ready(function() {
        setInterval(function() {
            $.get("scanStatistics", function(data, status) {
                drawTable(data);
            });
        }, 15000);
    });
}

function drawTable(data) {
    $(".statTable").remove();
    $(".stats").append("<table class='.statTable'></table>");
    $(".statTable").append(row);
    row.append($("<td>Site</td>"));
    row.append($("<td>Progress</td>"));
    for (var scanId in data['ascan']) {
        if (data.hasOwnProperty(scanId)) {
            drawRow(data[scanId]);
        }
    }
}

function drawRow(rowData) {
    var row = $("<tr />");
    $(".statTable").append(row);
    row.append($("<td>" + rowData[0] + "</td>"));
    row.append($("<td>" + rowData[1] + "</td>"));
}


function getScanStatus() {
    setInterval(function() {
        $.get('/admin/scanStatistics', function(data, status) {
            if (data.spider != 0){
                $('.prog').empty();
                $('.prog').append("<label for='spider'>Spidering</label>");
            }
            else {
                for (var scan in data.ascan) {
                    if (data.ascan.hasOwnProperty(scan)) {
                        $('.prog').empty();
                        if (data.ascan[0][2] == 'FINISHED') {
                            $('.prog').append('Completed');
                        }
                        else {
                        $('.prog').append("<progress class='progBar' value='" + data.ascan[0][1] + "' max='100' />");
                        //$('.progBar').val(data.ascan[scan]);
                        }
                    }
                }
            }
        });
    }, 5000);
}
