""" admin controllers """
from pdb import set_trace
# Flask imports
from flask import Blueprint, render_template, session, request, \
                  redirect, url_for, flash, jsonify
from flask.ext.login import login_required, current_user

# Import models
from sentinel.auth.models import User
from sentinel.admin.models import Log

# Import forms
from sentinel.admin.forms import CreateUserForm, EditUserForm, ZapScanForm, AddSiteForm

# Import utils
from sentinel.utils.admin import (
    add_user_to_db, update_user, del_user, zap_scan, add_new_site,
    stat, stop_scan, zap_scan_without_login
)
from sentinel.utils.auth import get_user, check_admin, check_valid_username
from sentinel.utils.utils import exception_handler

from werkzeug.exceptions import InternalServerError

from sentinel import celery_app
from sentinel import app, db

# Create blueprint
admin = Blueprint('admin', __name__, url_prefix='/admin')


#*****************ROUTES*********************************#
@admin.route('/sites', methods=['GET', 'POST'])
@login_required
@check_admin(current_user)
def admin_home():
    """
    Returns the homepage for the admin user
    """
    try:
        form = AddSiteForm()
        
        if form.validate_on_submit():
            if add_new_site(current_user, form.url.data):
                flash(form.url.data + " added successfully.", "message")
            else:
                flash("Add website operation unsuccessful!", "error")

            return redirect(url_for("admin.admin_home"))

        return render_template("home/sites.html", user=current_user, form=form)
    except:
        raise InternalServerError


@admin.route('/sites/add', methods=['GET', 'POST'])
@login_required
@check_admin(current_user)
def add_site():
    """
    Adds a website to the admin
    """
    form = AddSiteForm()

    if form.validate_on_submit():
        if add_new_site(current_user, form.url.data):
            flash(form.url.data + " added successfully.", "message")
            return redirect(url_for("admin.admin_home"))
        else:
            flash("Add website operation unsuccessful!", "error")
            return redirect(url_for("admin.admin_home"))

    return render_template("admin/add_site.html", form=form)


@admin.route('/users', methods=['GET'])
@login_required
@check_admin(current_user)
def admin_users():
    """
    Returns the page listing all users and the sites they own
    """
    users = User.objects(isAdmin=False, isActive=True).only('username', 'ownedSites')
    return render_template("admin/admin_users.html", users=users)


@admin.route('/users/create_user', methods=['GET', 'POST'])
@login_required
@check_admin(current_user)
def create_user():
    """
    GET:
    Returns the create user form
    POST:
    Validate the user creation form and creates an user in db.
    If the operation fails, the error message is flashed, else 
    its redirected to 'admin_users' page.
    """
    form = CreateUserForm()

    form_choices = [(site, site) for site in current_user.ownedSites]
    form.websites.choices = form_choices

    #********POST*****************************************#
    if form.validate_on_submit():
        usrname = form.username.data
        passwd = form.password.data
        websites = form.websites.data
        if add_user_to_db(usrname, passwd, websites):
            return redirect(url_for('admin.admin_users'))
        else:
            flash('Username already exists', 'error')
    #*****************************************************#

    return render_template("admin/create_user.html", form=form)


@admin.route('/users/<username>/edit', methods=['GET', 'POST'])
@login_required
@check_admin(current_user)
@check_valid_username
def edit_user(user):
    """
    GET:
    Returns the edit user form (for adding and removing websites).
    POST:
    Validate the form and edits the 'ownedSites' field of an user in db.
    If the operation fails, it will flash an error message on the form page, else
    its redirected to 'admin_users' page.
    """
    form = EditUserForm()

    form.to_delete.choices = [(site, site) for site in user.ownedSites]

    form.to_add.choices = [(site, site) for site in current_user.ownedSites 
                           if site not in user.ownedSites]

    #***********POST*********************************************************#
    if form.validate_on_submit():
        new_sites_list = [site for site in user.ownedSites 
                          if site not in form.to_delete.data] + form.to_add.data

        if update_user(user, new_sites_list):
            return redirect(url_for("admin.admin_users"))
        flash("Update operation failed! Please try again.", "error")
    #************************************************************************#

    return render_template("admin/edit_user.html", form=form, username=user.username)


@admin.route('/users/<username>/delete', methods=['GET'])
@login_required
@check_admin(current_user)
@check_valid_username
def delete_user(user):
    """
    Checks if the given username is valid and deletes the given user.
    It the operation is successful, it redirects to 'admin_users' page, else,
    it flashes an error message
    """
    if not user.isAdmin and not del_user(user):
        flash("Delete operation failed", "error")
    elif user.isAdmin:
        flash("Cannot delete admin user", "error")
    else:
        flash("Deleted user %s" %(user.username))

    return redirect(url_for("admin.admin_users"))


@admin.route('/scan_website', methods=['GET', 'POST'])
@login_required
@check_admin(current_user)
def scan_website():
    """
    Initiates a ZAP scan on the url submitted via the form, only if there is
    no ongoing ZAP scan. The zap_scan function is handled by celery.
    """
    try:
        form = ZapScanForm()
        form.url.choices = [(site, site) for site in current_user.ownedSites]

        #scan_state = celery_app.AsyncResult(str(app.config['SCAN_ID'])).state

        #if scan_state == 'PROGRESS':
        #    form = None
        #    flash("Ongoing scan. Cannot initiate a new scan.", "message")

        scan_state = stat()
        if scan_state['spider'] or scan_state['ascan']:
            form = None
            flash("Ongoing scan. Cannot initiate a new scan.", "message")

        #*****************POST*******************************************#
        if form and form.validate_on_submit():
            url = form.url.data
            pre_login = form.pre_login.data
            db_conn = db.connection.sentinel_app

            scan_details = db_conn.scanProfiles.find_one({'url': url})
            if not scan_details and url[-1] != '/':
                scan_details = db_conn.scanProfiles.find_one({'url': url+'/'})

            if not scan_details and url[-1] == '/':
                scan_details = db_conn.scanProfiles.find_one({'url': url[:-1]})

            if not scan_details:
                flash("No scan profile in the database!", "error")
                return redirect(url_for("admin.admin_home"))

            api_key = app.config['ZAP_APIKEY']
            target = scan_details['target_url']
            spider_seed = scan_details['spider_seed']
            context = scan_details['context_url']
            if not pre_login:
                login = scan_details['login_url']
                logout = scan_details['logout_url']
                username = scan_details['username']
                username_tag = scan_details['username_tag']
                password = scan_details['password']
                password_tag = scan_details['password_tag']

                if login:
                    scan_id = zap_scan.apply_async([{"api_key": api_key,
                                                    "target": target,
                                                    "spider_seed": spider_seed,
                                                    "context_url": context,
                                                    "login_indicator": login,
                                                    "logout_indicator": logout,
                                                    "username": username,
                                                    "username_tag": username_tag,
                                                    "password": password,
                                                    "password_tag": password_tag}
                                                ])
                else:
                    scan_id = zap_scan_without_login.apply_async([{
                        "api_key": api_key,
                        "target": target,
                        "spider_seed": spider_seed,
                        "context_url": context
                    }])
            else:
                scan_id = zap_scan_without_login.apply_async([{
                    "api_key": api_key,
                    "target": target,
                    "spider_seed": spider_seed,
                    "context_url": context
                }])


            app.config['SCAN_ID'] = scan_id

            flash("Initiated ZAP scan for the URL :- %s" %(target), "message")
            return redirect(url_for('admin.admin_home'))
        #****************************************************************#

        return render_template('admin/zap_scan.html', form=form)
    except IOError:
        return "ZAP service currently not available"

@admin.route('/logs', methods=['GET'])
@login_required
@check_admin(current_user)
def logs():
    """
    Returns a page showing the log entries
    """
    log_entries = Log.objects().order_by('-timestamp')
    return render_template("/admin/logs.html", entries=log_entries)


@admin.route('/getScanState', methods=['GET'])
@login_required
@check_admin(current_user)
def check_scan_state():
    """
    """
    scan_state = celery_app.AsyncResult(str(app.config['SCAN_ID'])).state
    if scan_state == 'FAILURE':
        app.config['SCAN_ID'] = None
    return jsonify({'status' : scan_state})


@admin.route('/scanStatistics', methods=['GET'])
@login_required
@check_admin(current_user)
def scan_statistics():
    """
    """
    return jsonify(stat())


@admin.route('/stop_scan', methods=['GET'])
@login_required
@check_admin(current_user)
def stop_zap_scan():
    try:
        stop_status = stop_scan()
        if stop_status["status"] == "SUCCESS":
            flash("Scan stopped", "message")
            return redirect(url_for('admin.scan_website'))
        elif stop_status["status"] == "NOSCAN":
            flash("No scans running currently", "message")
            return redirect(url_for('admin.scan_website'))
        elif stop_status["status"] == "FAILURE":
            flash("Error stopping scan", "message")
            return redirect(url_for('admin.scan_website'))
    except IOError:
        return "ZAP service currently not available"
