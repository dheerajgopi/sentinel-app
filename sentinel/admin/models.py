from sentinel import db

class Log(db.Document):
    user = db.StringField()
    event = db.StringField()
    timestamp = db.DateTimeField()
