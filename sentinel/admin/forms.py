#from flask.ext.login import current_user

from flask.ext.wtf import Form

from wtforms import (
    TextField, PasswordField, SelectMultipleField, SelectField,
    widgets, BooleanField
)

from wtforms.validators import Required

class CreateUserForm(Form):
    username = TextField('Username', [Required(message='Username required')])
    password = PasswordField('Password', [Required(message='Password required')])
    websites = SelectMultipleField('Websites', choices=[])
                                   #widget = widgets.ListWidget(prefix_label=False),
                                   #option_widget=widgets.CheckboxInput())

class EditUserForm(Form):
    to_delete = SelectMultipleField('Delete', choices=[])
    to_add = SelectMultipleField('Add', choices=[])

class ZapScanForm(Form):
    url = SelectField('url')
    pre_login = BooleanField('pre_login')

class AddSiteForm(Form):
    url = TextField('Site URL', [Required(message='URL Required')])
