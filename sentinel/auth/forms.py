from flask.ext.wtf import Form

# Import Form elements such as TextField
from wtforms import TextField, PasswordField

# Import Form validators
from wtforms.validators import Required


# Define the login form (WTForms)

class LoginForm(Form):
    """
    Log in form.
    """
    username = TextField('Username', [
                Required(message='Forgot your Username?')])
    password = PasswordField('Password', [
                Required(message='Must provide a password!')])

