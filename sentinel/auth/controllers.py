""" auth controllers """

# Flask imports
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for
from flask.ext.login import login_user , logout_user , current_user , login_required

# Import db and login manager
from sentinel import db, login_manager

# Import forms
from sentinel.auth.forms import LoginForm

# Import models

# Import utilities
from sentinel.utils.auth import get_user, check_password
from sentinel.utils.admin import log_entry

# Create blueprint
auth = Blueprint('auth', __name__)

# Other imports
from datetime import datetime


#***************ROUTES**************************************#
@auth.route('/', methods=['GET', 'POST'])
@auth.route('/login/', methods=['GET', 'POST'])
def login():
    """
    The login controller handles user login.
    GET Method:
    If there is an active session, the controller redirects to the homepage
    of the user in the current session, or it will show the login page.
    POST Method:
    When user submits the login form, the credentials are checked, and a new
    session is created before logging in the user to their home page.
    """
    if session.get('username'):
        username = session.get('username')
        nextpage = request.args.get('next')
        return redirect(nextpage or url_for('home.homepage', username=username))

    # Login Form
    form = LoginForm(request.form)

    #***************POST*****************************************#
    if form.validate_on_submit():
        user = get_user(form.username.data)
        # authenticate the user and create the session
        if user and check_password(user, form.password.data):
            login_user(user)
            session['username'] = user.username
            log_entry(current_user.username, "logged in", datetime.now())
            flash('Welcome %s!' % user.username, 'message')
            nextpage = request.args.get('next')
            return redirect(nextpage or url_for('home.homepage'))
        flash('Wrong username or password', 'error')
    #************************************************************#

    return render_template("auth/login.html", form=form)

# The page to show when we try to access an users page directly from the
# address bar of browser, without logging in
login_manager.login_view = 'auth.login'


@login_manager.user_loader
def load_user(username):
    """
    Helper function for login manager.
    Loads User object from db.
    """
    try:
        user = get_user(username)
        return user
    except:
        raise


@auth.route("/logout")
@login_required
def logout():
    """
    Logs out user and ends the session
    """
    log_entry(current_user.username, "logged out", datetime.now())
    logout_user()
    session.pop('username', None)
    return redirect(url_for('auth.login'))
