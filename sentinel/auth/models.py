""" models used by the sentinel application """

from sentinel import db
from datetime import datetime

class User(db.Document):
    """ user of the application """
    username = db.StringField(unique=True, required=True)
    password = db.StringField(required=True)
    isAdmin = db.BooleanField(default=False)
    ownedSites = db.ListField(default=[])
    dateCreated = db.DateTimeField(default=datetime.now())
    isActive = db.BooleanField(default=True)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.username)

    def __repr__(self):
        return '<User %r>' % (self.username)
