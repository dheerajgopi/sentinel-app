""" utility functions for home """
import sys


def get_alerts(report):
    """ get list of alerts from report """
    if isinstance(report['site'], list):
        site = report['site'][0]
    else:
        site = report['site']
    try:
        alerts = site['alerts']['alertitem']
    except TypeError:
        alerts = []
    return alerts


def risk_histogram(alerts):
    """ create a histogram of low, medium, and high risk alerts 
        :alerts a list of alerts (dicts)
        :risk_hist a dict histogram of risklevels
    """
    risk_hist = {'low':0, 'medium':0, 'high':0}
    for alert in alerts:
        if alert['riskcode'] == '1':
            risk_hist['low'] += 1
        elif alert['riskcode'] == '2':
            risk_hist['medium'] += 1
        elif alert['riskcode'] == '3':
            risk_hist['high'] += 1
    return risk_hist


def convert_histogram(hist):
    """ convert a histogram to percentage 
    :hist a dict histogram with integer counts
    :hist_pcent a histogram with percentage values
    """
    total = sum(hist.values())
    hist_pcent = {key: hist[key] / float(total) for key in hist.keys()}
    return hist_pcent


def exception_handler(*exceptions):
    def _exception_handler(func):
        def inner(*args, **kwargs):
            try:
                result = func(*args, **kwargs)

            except (exceptions) as err:
                return "Internal server error - " + str(err), 500

            except:
                from traceback import print_tb
                return sys.exc_info()[0]
            return result
        return inner
    return _exception_handler
