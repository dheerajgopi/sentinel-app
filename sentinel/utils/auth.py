from sentinel.auth.models import User
from flask.ext.login import current_user
from functools import wraps
from pymongo.errors import AutoReconnect, ServerSelectionTimeoutError

# Import password / encryption helper tools
from werkzeug import check_password_hash

def get_user(username):
    """
    Gets the user object from db, when username is passed
    """
    try:
        return User.objects(username=username)[:1][0]
    except IndexError:
        return None

def check_password(user, password):
    """
    Returns True if password is valid, else returns False
    """
    return check_password_hash(user.password, password)

def check_admin(user):
    """
    decorator to check if the current user has admin privilage.
    If the user is not an admin, they cannot access admin functionalities.
    """
    def check_admin_decorator(func):
        @wraps(func)
        def func_wrapper(*args, **kwargs):
            """
            wrapper function for check_admin decorator
            """
            if user.isAdmin:
                return func(*args, **kwargs)
            return "Unauthorised"
        return func_wrapper
    return check_admin_decorator

def check_valid_username(view):
    """
    decorator to check if the username passed as variable url parameter
    is a valid one. It returns the view function if username is valid, else
    it returns "Invalid username"
    """
    @wraps(view)
    def view_wrapper(*args, **kwargs):
        """
        wrapper function for check_valid_username decorator
        """
        user = kwargs['user'] = get_user(kwargs.pop('username', None))

        if not user:
            return "Invalid Username"

        return view(*args, **kwargs)
    return view_wrapper
