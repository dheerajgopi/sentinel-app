from sentinel.home.models import Report
from time import strptime

def fetch_report(site, date):
    """
    Searches and pulls out the report document from mongodb
    based on the site and date given as arguments
    """
    try:
        report = Report.objects(__raw__={'OWASPZAPReport.site.@name': site,
                                         'OWASPZAPReport.@generated': date})
        if not report:
            report = Report.objects(__raw__={'OWASPZAPReport.site.@name': site + '/',
                                             'OWASPZAPReport.@generated': date})
        if not report and site[-1] == '/':
            report = Report.objects(__raw__={'OWASPZAPReport.site.@name': site[:-1],
                                             'OWASPZAPReport.@generated': date})

        return report[0]
    except IndexError:
        return None

def fetch_alert_list(report_id):
    """
    Calls fetch_report to get the full report and processes it so that
    is can be easily displayed in a template.
    """
    report = Report.objects(id=report_id).first().OWASPZAPReport

    alert_list = []
    for alert in report['site']['alerts']['alertitem']:
        alert_instance = alert['instances']['instance']
        if isinstance(alert_instance, dict):
            alert_instance = [alert_instance]
            alert['instances']['instance'] = alert_instance

        alert_list.append(alert)

    report['site']['alerts']['alertitem'] = alert_list
    return report


def to_time(time_tup):
    """ converts the time string to time struct """
    try:
        time_struct = strptime(time_tup[0], '%a, %d %b %Y %H:%M:%S')
    except ValueError:
        gmt_idx = time_tup[0].find(' GMT')
        time_str = time_tup[0][:gmt_idx].replace(' ', ', ', 1)
        try:
            time_struct = strptime(time_str, '%a, %d %b %Y %H:%M:%S')
        except ValueError:
            time_struct = strptime(time_str, '%a, %b %d %Y %H:%M:%S')
    return time_struct
    

def fetch_scan_dates(site):
    """
    Returns a list of dates on which the ZAP scan was conducted for the
    particular site given as argument.
    """
    scans = Report.objects(__raw__={'OWASPZAPReport.site.@name': site})
    if not scans:
        scans = Report.objects(__raw__={'OWASPZAPReport.site.@name': site + '/'})
    if not scans and site[-1] == '/':
        scans = Report.objects(__raw__={'OWASPZAPReport.site.@name': site[:-1]})
    dates = []
    for scan in scans:
        date = scan['OWASPZAPReport']['@generated']
        dates.append((date, date))

    dates.sort(key=to_time, reverse=True)
    return dates
