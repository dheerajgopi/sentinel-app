from flask import abort
from sentinel import app
from uuid import uuid4
from datetime import datetime, timedelta
from flask.sessions import SessionInterface, SessionMixin
from werkzeug.datastructures import CallbackDict
from werkzeug.exceptions import InternalServerError
from pymongo import MongoClient


class MongoSession(CallbackDict, SessionMixin):

    def __init__(self, initial=None, sid=None):
        CallbackDict.__init__(self, initial)
        self.sid = sid
        self.modified = False


class MongoSessionInterface(SessionInterface):

    def __init__(self, host=app.config['MONGODB_HOST'], port=app.config['MONGODB_PORT'],
                 db='', username=app.config['MONGODB_USERNAME'],
                 password=app.config['MONGODB_PASSWORD'], collection='sessions'):
        client = MongoClient('mongodb://sentinel:sentinel@127.0.0.1:27017/sentinel_app')
        self.store = client[db][collection]

    def open_session(self, app, request):
        try:
            sid = request.cookies.get(app.session_cookie_name)
            if sid:
                stored_session = self.store.find_one({'sid': sid})
                if stored_session:
                    if stored_session.get('expiration') > datetime.utcnow():
                        return MongoSession(initial=stored_session['data'],
                                            sid=stored_session['sid'])
            sid = str(uuid4())
            return MongoSession(sid=sid)
        except:
            raise InternalServerError

    def save_session(self, app, session, response):
        domain = self.get_cookie_domain(app)
        #if not session:
        #    response.delete_cookie(app.session_cookie_name, domain=domain)
        #    return
        if self.get_expiration_time(app, session):
            expiration = self.get_expiration_time(app, session)
        else:
            expiration = datetime.utcnow() + timedelta(hours=1)
        self.store.update({'sid': session.sid},
                          {'sid': session.sid,
                           'data': session,
                           'expiration': expiration}, True)
        response.set_cookie(app.session_cookie_name, session.sid,
                            expires=self.get_expiration_time(app, session),
                            httponly=True, domain=domain)

