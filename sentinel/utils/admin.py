from celery.contrib.rdb import set_trace
# Import models
from sentinel.auth.models import User
from sentinel.admin.models import Log
from sentinel.home.models import Report

# Import exceptions
from mongoengine.errors import NotUniqueError

# Import function for generating password hash
from werkzeug import generate_password_hash

# For accessing SCAN_ID in config
from sentinel import app

# Celery and ZAP related
import time
from zapv2 import ZAPv2
from sentinel import celery_app
from sentinel.utils.xmltojson import xmltojson_str


# Initiating ZAP client
zap = ZAPv2(proxies={'http': app.config['ZAP_PROXY'], 'https': app.config['ZAP_PROXY']})


def add_user_to_db(usrname, passwd, websites):
    """
    Adds an user object to 'user' collection.
    Returns True if write operation succeeds, else it returns False
    """
    user = User(username=usrname,
                password=generate_password_hash(passwd),
                ownedSites=websites)
    try:
        user.save()
        return True

    except NotUniqueError:
        return False

def update_user(user, websites):
    """
    Updates the 'ownedSites' field of an user document in 'user' collection.
    Returns True if update succeeds, else it returns False
    """
    try:
        user.update(ownedSites=websites)
        return True
    except:
        return False

def del_user(user):
    """
    Deletes an user to 'user' collection.
    Returns True if delete operation succeeds, else it returns False
    """
    try:
        user.update(isActive=False)
        return True
    except:
        return False

def add_new_site(user, url):
    """
    """
    try:
        user.update(add_to_set__ownedSites=[url])
        return True
    except:
        return False

def log_entry(username, event, timestamp):
    """
    Adds an log entry to the 'log' collection.
    """
    log = Log(user=username,
              event=event,
              timestamp=timestamp)

    log.save()


@celery_app.task()
def zap_scan_without_login(params):
    """
    Adds a task to the task queue in celery worker instance, updates the SCAN_ID
    variable in config to the celery task id. The celery worker then initiates ZAP
    scan on the given url, generates the report and insert it to DB.
    """
    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider1', 'progress': 0})
    context = '\Q' + params["context_url"] + '\E.*'
    zap = ZAPv2(proxies={'http': app.config['ZAP_PROXY'], 'https': app.config['ZAP_PROXY']})
    zap.core.new_session(apikey=params["api_key"])
    print 'wl Accessing target %s' % params["target"]
    zap.urlopen(params["target"]) 
    # Give the sites tree a chance to get updated
    time.sleep(2)

    context_id = zap.context.new_context('scan', params["api_key"])
    zap.context.include_in_context('scan', context, params["api_key"])
    zap.spider.exclude_from_scan('.*mozilla.*', params["api_key"])
    zap.spider.exclude_from_scan('.*google.*', params["api_key"])
    zap.ascan.exclude_from_scan('.*mozilla.*', params["api_key"])
    zap.ascan.exclude_from_scan('.*google.*', params["api_key"])

    # Start spidering
    print 'wl Spidering target %s' % params["target"]
    spider_scan_id = zap.spider.scan(params["target"], recurse=True, apikey=params["api_key"])
    # Give the Spider a chance to start
    time.sleep(10)
    while (int(zap.spider.status(spider_scan_id)) < 100):
        print 'wl Spider progress %: ' + zap.spider.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider1', 'progress': int(zap.spider.status(spider_scan_id))})
        time.sleep(2)

    # Start spidering Again
    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider2', 'progress': 0})
    spider_scan_id = zap.spider.scan(params["target"], recurse=True, apikey=params["api_key"])
    # Give the Spider a chance to start
    time.sleep(10)
    while (int(zap.spider.status(spider_scan_id)) < 100):
        print 'wl Spider progress %: ' + zap.spider.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider2', 'progress': int(zap.spider.status(spider_scan_id))})
        time.sleep(2)

    print 'wl Spider completed'
    # Give the passive scanner a chance to finish
    time.sleep(5)

    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'ascan', 'progress': 0})

    print 'wl Scanning target %s' % params["target"]
    active_scan_id = zap.ascan.scan(params["target"], True, apikey=params["api_key"])
    while (int(zap.ascan.status(active_scan_id)) < 100):
        print 'wl Scan progress %: ' + zap.ascan.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'ascan', 'progress': int(zap.ascan.status(active_scan_id))})
        time.sleep(5)

    print 'wl Scan completed'

    print 'wl Hosts: ' + ', '.join(zap.core.hosts)
    xml = zap.core.xmlreport(apikey=params["api_key"])
    report_json = xmltojson_str(xml)
    Report().insert(report_json)

#    except:
#        celery_app.current_task.update_state(state='ZAP_CONN_REFUSED')


@celery_app.task()
def zap_scan(params):
    """
    Adds a task to the task queue in celery worker instance, updates the SCAN_ID
    variable in config to the celery task id. The celery worker then initiates ZAP
    scan on the given url, generates the report and insert it to DB.
    """
    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider1', 'progress': 0})
    context = '\Q' + params["context_url"] + '\E.*'
    logout = '\Q' + params["logout_indicator"] + '\E'
    authmethodconfig = 'loginUrl=' + params["login_indicator"] + '&loginRequestData=' + params["username_tag"] + '%3D%7B%25username%25%7D%26' + params["password_tag"] + '%3D%7B%25password%25%7D'
    auth_credentials = 'username=' + params["username"] + '&password=' + params["password"]


    # zap = ZAPv2()
    # Use the line below if ZAP is not listening on 8090
    zap = ZAPv2(proxies={'http': app.config['ZAP_PROXY'], 'https': app.config['ZAP_PROXY']})
    # create new session
    zap.core.new_session(apikey=params["api_key"])
    # do stuff
    print 'Accessing target %s' % params["target"]
    # try have a unique enough session...
    zap.urlopen(params["target"]) 
    # Give the sites tree a chance to get updated
    time.sleep(2)

    context_id = zap.context.new_context('scan', params["api_key"])
    zap.context.include_in_context('scan', context, params["api_key"])
    zap.authentication.set_authentication_method(context_id ,'formBasedAuthentication', authmethodconfig, params["api_key"])
    zap.authentication.set_logged_in_indicator(context_id, logout, params["api_key"])
    user_id = zap.users.new_user(context_id, 'user', params["api_key"])
    zap.users.set_user_enabled(context_id, user_id, True, params["api_key"])
    zap.users.set_authentication_credentials(context_id, user_id, auth_credentials, params["api_key"])
    #zap.forcedUser.set_forced_user(context_id, user_id, params["api_key"])
    #zap.forcedUser.set_forced_user_mode_enabled(True, params["api_key"])


    print 'Spidering target %s' % params["target"]

    zap.spider.exclude_from_scan('.*mozilla.*', params["api_key"])
    zap.spider.exclude_from_scan('.*google.*', params["api_key"])
    zap.ascan.exclude_from_scan('.*mozilla.*', params["api_key"])
    zap.ascan.exclude_from_scan('.*google.*', params["api_key"])

    # Start spidering
    spider_scan_id = zap.spider.scan_as_user(params["spider_seed"], context_id, user_id, recurse=True, apikey=params["api_key"])
    # Give the Spider a chance to start
    time.sleep(10)
    while (int(zap.spider.status(spider_scan_id)) < 100):
        print 'Spider progress %: ' + zap.spider.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider1', 'progress': int(zap.spider.status(spider_scan_id))})
        time.sleep(2)

    # Start spidering Again
    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider2', 'progress': 0})
    spider_scan_id = zap.spider.scan_as_user(params["spider_seed"], context_id, user_id, recurse=True, apikey=params["api_key"])
    # Give the Spider a chance to start
    time.sleep(10)
    while (int(zap.spider.status(spider_scan_id)) < 100):
        print 'Spider progress %: ' + zap.spider.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'spider2', 'progress': int(zap.spider.status(spider_scan_id))})
        time.sleep(2)

    print 'Spider completed'
    # Give the passive scanner a chance to finish
    time.sleep(5)

    celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'ascan', 'progress': 0})

    print 'Scanning target %s' % params["target"]
    active_scan_id = zap.ascan.scan_as_user(params["target"], context_id, user_id, True, apikey=params["api_key"])
    while (int(zap.ascan.status(active_scan_id)) < 100):
        print 'Scan progress %: ' + zap.ascan.status()
        celery_app.current_task.update_state(state='PROGRESS', meta={'process': 'ascan', 'progress': int(zap.ascan.status(active_scan_id))})
        time.sleep(5)

    print 'Scan completed'

    # Report the results

    print 'Hosts: ' + ', '.join(zap.core.hosts)
    xml = zap.core.xmlreport(apikey=params["api_key"])
    report_json = xmltojson_str(xml)
    Report().insert(report_json)

#    except:
#        celery_app.current_task.update_state(state='ZAP_CONN_REFUSED')
    

def stat():
    spider_list = [spider for spider in zap.spider.scans if spider['state'] == 'RUNNING']
    ascan_list = [scan for scan in zap.ascan.scans if scan['state'] == 'RUNNING']
    scan_stat = {}
    for scan in ascan_list:
        site = zap.ascan.scan_progress(int(scan['id']))[0]
        scan_stat[scan['id']] = [site, scan['progress'], scan['state']]

    return {'spider': len(spider_list), 'ascan': scan_stat}


def stop_scan():
    """
    Stops current scan and starts a new session
    """
    try:
        result = False

        if zap.ascan.stop(0, app.config["ZAP_APIKEY"]) == 'OK':
            result = True

        elif zap.spider.stop(1, app.config["ZAP_APIKEY"]) == 'OK':
            result = True

        elif zap.spider.stop(0, app.config["ZAP_APIKEY"]) == 'OK':
            result = True

        if result:
            zap.core.new_session(apikey=app.config["ZAP_APIKEY"])
            #celery_app.control.revoke(str(app.config['SCAN_ID']), terminate=True)
            return {"status": "SUCCESS"}

        else:
            return {"status": "NOSCAN"}

    except Exception as err:
        print "error: %s" % err
        return {"status": "FAILURE"}
