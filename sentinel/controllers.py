from flask import Blueprint, redirect, url_for

homepage = Blueprint('homepage', __name__, url_prefix='/')

@homepage.route('/')
def index_page():
    return redirect(url_for('auth.login'))
