#Import flask and template operators
from flask import Flask, render_template

# Import MongoEngine
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager

from pymongo.errors import AutoReconnect, ServerSelectionTimeoutError
from werkzeug.exceptions import HTTPException
# Celery
from celery import Celery

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# flask-login setup
login_manager = LoginManager()
login_manager.init_app(app)

# Define the database object which is imported
# by modules and controllers
db = MongoEngine(app)

# Define celery object
celery_app = Celery(app.name, broker=app.config['BROKER_URL'])
celery_app.conf.update(app.config)

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def db_server_timeout_error(error):
    return render_template('500.html'), 500

# Register blueprint(s)
def register_blueprints(app):
    """ Register blueprints of the app """
    from sentinel.auth.controllers import auth
    from sentinel.home.controllers import home
    from sentinel.admin.controllers import admin
    from sentinel.controllers import homepage
    app.register_blueprint(auth)
    app.register_blueprint(home)
    app.register_blueprint(admin)
    app.register_blueprint(homepage)

# Initiate session
def init_session(app):
    from sentinel.utils.session import MongoSessionInterface
    # Session interface with mongodb
    app.session_interface = MongoSessionInterface(db=app.config['MONGODB_DB'])

register_blueprints(app)
init_session(app)

@app.after_request
def set_headers(response):
    response.headers['X-XSS-Protection'] = 1
    return response

