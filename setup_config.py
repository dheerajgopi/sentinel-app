#! virtenv/bin/python
""" modifies the config.py of the sentinel app """
from werkzeug import generate_password_hash
from pymongo import MongoClient
from getpass import getpass
from datetime import datetime


def get_settings():
    """ gets the settings from the user 
    mongodb_db = data base name
    mongodb_host = x.x.x.x
    mongodb_port = port
    zap_proxy = http://x.x.x.x:port
    zap_apikey = the_key
    """
    mongodb_db = raw_input("enter mongodb database name: ")
    mongodb_host = raw_input("enter mongodb host: ")
    mongodb_port = raw_input("enter mongodb port: ")
    mongodb_username = raw_input("enter mongodb databse username: ")
    mongodb_password = getpass("enter mongodb databse username: ")
    csrf_session_key = getpass("enter csrf_session_key: ")
    secret_key = getpass("enter app secret key: ")
    zap_proxy = "http://" + raw_input("enter zap host and port "
            "(eg: 172.13.4.12:5000): "
    )
    zap_apikey = raw_input("enter zap apikey: ")
    
    return {
            "mongodb_db": mongodb_db,
            "mongodb_host": mongodb_host,
            "mongodb_port": mongodb_port,
            "mongodb_username": mongodb_username,
            "mongodb_password": mongodb_password, 
            "csrf_session_key": csrf_session_key,
            "secret_key": secret_key,
            "zap_proxy": zap_proxy,
            "zap_apikey": zap_apikey
    }



def edit(file_name, settings):
    """ edits the file with the settings """
    with open(file_name, 'rw') as config:
        lines = config.readlines()
        for idx, line in enumerate(lines):
            if line.startswith('MONGODB_DB'):
                lines[idx] = 'MONGODB_DB = "' + settings['mongodb_db'] + '"\n'
            if line.startswith('MONGODB_HOST'):
                lines[idx] = 'MONGODB_HOST = "' + settings['mongodb_host'] + '"\n'
            if line.startswith('MONGODB_PORT'):
                lines[idx] = 'MONGODB_PORT = ' + settings['mongodb_port'] + '\n'
            if line.startswith('MONGODB_USERNAME'):
                lines[idx] = 'MONGODB_USERNAME = "' + settings['mongodb_username'] + '"\n'
            if line.startswith('MONGODB_PASSWORD'):
                lines[idx] = 'MONGODB_PASSWORD = "' + settings['mongodb_password'] + '"\n'
            if line.startswith('ZAP_PROXY'):
                lines[idx] = 'ZAP_PROXY = "' + settings['zap_proxy'] + '"\n'
            if line.startswith('ZAP_APIKEY'):
                lines[idx] = 'ZAP_APIKEY = "' + settings['zap_apikey'] + '"\n'

    with open(file_name, 'w') as config:
        for line in lines:
            print line
            config.write(line)


def create_admin(settings):
    """ creates the admin in mongodb """
    mongo_url = ("mongodb://" + settings['mongodb_username'] + ":" + settings['mongodb_password']
            + '@' + settings['mongodb_host'] + ":" + settings['mongodb_port'] + "/"
            + settings['mongodb_db'])
    print mongo_url
    #client = MongoClient('mongodb://sentinel:sentinel@127.0.0.1:27017/sentinel_app')
    client = MongoClient(mongo_url)
    db = client[settings['mongodb_db']]
    collection = db.user
    username = raw_input("enter admin user name: ")
    password = generate_password_hash(getpass("enter admin password: "))
    collection.insert_one({
        "username": username,
        "password": password,
        "isAdmin": True,
        "isActive": True,
        "ownedSites": [],
        "dateCreated": datetime.now()
    })


if __name__ == "__main__":
    settings = get_settings()
    edit("config.py", settings)
    create_admin(settings)
