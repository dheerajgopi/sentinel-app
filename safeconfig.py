# Statement for enabling the development environment
DEBUG = False

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Database config if any
MONGODB_DB = ""
MONGODB_HOST = "" 
MONGODB_PORT = 27017
MONGODB_USERNAME = ""
MONGODB_PASSWORD = ""

# Config for celery
CELERY_RESULT_BACKEND = "mongodb://" +\
             MONGODB_USERNAME + ":" + MONGODB_PASSWORD + "@" +\
             MONGODB_HOST +\
             ":" +\
             str(MONGODB_PORT)
CELERY_MONGODB_BACKEND_SETTINGS = {
                "database": MONGODB_DB,
                "taskmeta_collection": "stock_taskmeta_collection"
                }

# For adding tasks to queue
CELERY_ALWAYS_EAGER = False
CELERY_TRACK_STARTED = True

BROKER_URL = "mongodb://" +\
             MONGODB_USERNAME + ":" + MONGODB_PASSWORD + "@" +\
             MONGODB_HOST +\
             ":" +\
             str(MONGODB_PORT) +\
             "/" +\
             MONGODB_DB

# Application threads.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = os.urandom(24)

# Secret key for signing cookies
SECRET_KEY = os.urandom(24)

# To store celery task id
SCAN_ID = None

SESSION_COOKIE_NAME = "PYTHSESSION"

PROPAGATE_EXCEPTIONS = True
TRAP_HTTP_EXCEPTIONS = False

ZAP_PROXY = "http://127.0.0.1:8080"
ZAP_APIKEY = ""
